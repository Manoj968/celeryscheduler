from celery.schedules import crontab
from django.shortcuts import render
from .tasks import test_func
from django.http import HttpResponse
from celery_email.tasks import send_mail_func
from django_celery_beat.models import PeriodicTask,CrontabSchedule
import json
# Create your views here.
def test(request):
    test_func.delay()
    return HttpResponse("Done")

def send_mail_to_all(request):
    send_mail_func.delay()
    return HttpResponse("Send")

def schedule_mail(request):
    schedule, created = CrontabSchedule.objects.get_or_create(hour = 18, minute = 10)
    task = PeriodicTask.objects.create(crontab=schedule, name="schedule_mail_task_"+"4", task='celery_email.tasks.send_mail_func') #,args= json.dumps(([[2,3]])))
    return HttpResponse("Done")