from django.urls import path, include
from .views import test,send_mail_to_all
from . import views
urlpatterns = [
    path('',views.test, name="test"),
    path('sendmail/',views.send_mail_to_all, name="sendmail"),
    path('schedule_mail/',views.schedule_mail, name="schedule_mail")
]